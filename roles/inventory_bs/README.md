
# inventory_bs role
This role gathers and parses the information from big switch controllers and their fabric devices (leaves and spines). 

## Host groups in inventory.ini
- site_bs

## Process
This role loops over ip addresses of the site and runs two distinct collection operations: one for the controller and the other for fabric devices.

### Controller
1. After loading variables from vault, the first block in `block.yml` collects information about the controller. We are interested in service tags and mac addresses among other attributes that may be useful. Each ip address we loop over belongs to a controller.
2. The first task is to gather the big switch controller data by logging into the device. All big switch devices are currently Dell devices. We run the following commands, parse their respective output and build a dictionary: 
  - "show environment"
  - "show version"
  - "enable"
     - "debug bash"
     - "ifconfig | grep bond0" (if BCF version is 4.0.0 and above) or
     - "ifconfig | grep eth0" (if BCF version 3.0.0 and above)
3. The data is then sent to the Inventory API with the following payload: 
 ```
ipAddress: "..."
site: "..."
collector: bigswitch
macAddress: "..."
inventoryInfo: {...}
```

### Fabric devices (Leaves and spines)
1. The second block in `block.yml` collects information about fabric devices. Similar to the controller we are interested in service tags and mac address among other attributes that may be useful. 
2. The first task we use the ip address to get a session cookie from the following url: 
```
https://{{ ip }}:8443/api/v1/auth/login
```
The session cookie is used to access the Big Switch API that returns a list of leaves and spines for the controller. The Big Switch API endpoint is:
```
https://{{ ip }}:8443/api/v1/data/controller/applications/bcf/info/fabric/switch
```
3. The mac address of each device enlisted in the JSON response from the Big Switch API endpoint is then extracted, reformatted and returned as a new list. 

4. To find the service tags we loop over mac address for every device and URL encode it to make a request to the following endpoint:
```
https://{{ ip }}:8443/api/v1/data/controller/core/zerotouch/device%5Bmac-address%3D%22{{ mac address }}%22%5D/action/status/environment/system-information
```
5. The dictionaries for each device mac address and service tag is combined and additional key value pairs are added `fabric_info` and `dev_info`.
6. Finally this data is posted to Inventory API with the following payload:
```
site: "..."
collector: bigswitch
macAddress: "..."
inventoryInfo:
  fabric_info: {...}
  device_info: {...}

```
## Command Explanations
### Controller
`show environment`: The output of this command is a table of details about the device ranging from fans to other units available. We extract the service tag from this output. 

`show version`: The output of this command returns the BCF version of the controller.

`"enable", "debug bash"...`: This series of commands enables the bash CLI and allows us to grab the mac address of the controller from the ifconfig table. 

## Custom Modules
The two custom modules used in the role are [formatter_bs_controller.py](https://github.vcp.vzwnet.com/VCP/inventory-automation/blob/master/library/format_bs_controller.py) to parse the controller data and [formatter_bs_macs.py](https://github.vcp.vzwnet.com/VCP/inventory-automation/blob/master/library/format_bs_macs.py) to reformat the mac address for fabric devices. 
