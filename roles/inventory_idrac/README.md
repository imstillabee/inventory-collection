# inventory_idrac role
This role collects information from Dell idracs returned from the `racadm hwinventory` command.

## Host groups
This role uses hosts created by the idrac_ips role, which pulls ip addresses from the formatter api.

## Process
The inventory_idrac role takes advantage of Ansible's ability to run in parallel to collect idrac information more quickly. Each of the hosts created by the idrac_ips role has a hostvar denoting the region. If the region is utility or security, this role will use one service account; if it's any other region, the role will use a different service account.

Several tasks after the collection deal with detecting collection failures. After that, the output from the idrac is parsed and sent to the formatter api.
