# inventory_cisco role
Collects inventory information about cisco devices.

## Host Groups in inventory.ini
This role relies on the `site_cisco` inventory group being present in a site group. Cisco devices are collected by ip address - if we don't have the ip in inventory.ini, then we aren't collecting the device.

## Process
This role runs the following process on every ip address defined in the inventory group:
1. Attempts to login using the two username/password combinations we have for the devices.
2. If successfully logged in, runs the following commands:
      - show inventory
      - show run
      - show diag | i Chassis
3. Parses the text output of those commands into JSON
4. Sends the JSON data to the formatter

## Custom Modules
This role relies on the custom module `format_cisco_show_run`, which parses the text output from the Cisco device into JSON data for the formatter to use.
