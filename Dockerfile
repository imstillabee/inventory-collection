FROM alpine:latest

MAINTAINER Drew Johnson <william.johnson8@verizonwireless.com>

RUN apk --update add sudo python py-pip nmap sshpass \
  && apk --update add openssl ca-certificates openssh \
  && apk --update add --virtual build-dependencies \
              python-dev libffi-dev openssl-dev build-base \
  && pip install --upgrade pip \
  && pip install ansible netmiko hvac

RUN mkdir /inventory-automation

WORKDIR /inventory-automation

COPY . /inventory-automation

CMD ["ansible-playbook", "--version"]
