inventory:
	$ ansible-playbook inventory.yml --extra-vars "site=$(SITE) ansible_python_interpreter=$(shell which python)"
inventory_idrac:
	$ ansible-playbook inventory_idrac.yml --extra-vars "site=$(SITE) ansible_python_interpreter=$(shell which python)"
site_push:
	$ ansible-playbook site_push.yml --extra-vars "site=$(SITE) ansible_python_interpreter=$(shell which python)"
inventory_storage:
	$ ansible-playbook run_role.yml --extra-vars "role=inventory_storage site=$(SITE)"
inventory_gigamon:
	$ ansible-playbook run_role.yml --extra-vars "role=inventory_gigamon site=$(SITE)"
discovery:
	$ ansible-playbook hp_discovery.yml --extra-vars "site=$(SITE) ansible_python_interpreter=$(shell which python)"
