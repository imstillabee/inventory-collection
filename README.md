# inventory-automation
Automated inventory collection.

## Setup
To run collectors from your machine, the minimum requirements are CMN connectivity, [Docker](https://docs.docker.com/engine/installation/), and [docker-compose](https://docs.docker.com/compose/install/). Advanced setup requires Ansible, Netmiko, and Nmap.

To run anything, a file called `vault-pw.txt` will need to be added in the project folder. This file contains the password used to decrypt the ansible-vault file containing usernames and passwords.

## Overall Inventory Process
Automated Live Inventory Collection needs to retrieve a variety of information from a variety of sources and make that information machine-readable (usually, by parsing a wall of text output into JSON). Our primary sources are HP 5900s, Dell iDRACs, the BigSwitch API, Cisco devices, Netapp devices, and Dell storage arrays. To collect information from these sources, ALIC relies on Ansible to orchestrate collection runs, several Ansible modules, and many custom Python Ansible modules, along with a large inventory.ini file to hold the information about site types and all the ip addresses that the playbooks need in order to run.

The first step in the collection process is to retrieve the mac-address tables, the arp tables, and the lldp tables from the site and TOR HP 5900 switches. This information is parsed into json, then sent to the inventory formatter api for the logic that transforms it into a basic map of the devices at a site.

Devices are collected in different ways, but we must be able to retrieve a mac address for all devices, since that is what the formatter api uses to pair devices with information retrieved from the HP 5900s. In general, we use known ip addresses to collect information - for example, the big switch controllers are assigned ips according to a schema. We use that ip to retrieve information about all devices on that controller's fabric via the bigswitch api.

Collectors run as scheduled by a cronjob. Most collectors take no more than an hour to complete Each collector spins up a docker container, runs ansible out of that container, and shuts down when it is complete. Logs are saved to log files named site-date.log. Every 24 hours, a cleaner script cleans up all docker containers and rebuilds the base image.

*tl;dr*: The inventory collectors run on a daily schedule to retrieve responses from equipment, convert them to json format when necessary, and send the json to the inventory formatter api.

### Docker
Docker provides a consistent, controlled environment for the ansible playbooks running in deployment with all dependencies tracked and installed. The Dockerfile shows all the dependencies required for the Ansible playbooks and modules to successfully run. This means that the only real requirement for a deployment instance is the ability to run Docker.

### inventory.ini
`inventory.ini` is the ansible hosts file for the collectors. It is organized according to site and device type. For example, the Amsterdam site is a group that contains other host groups divided according to device type and the needs of different playbooks.
```
[amsterdam:children]
amsterdam_hp
amsterdam_tors
amsterdam_bs
amsterdam_cisco
amsterdam_idrac
amsterdam_netapp
amsterdam_gigamon
```
The `amsterdam` group also contains group variables defining the type of site (`lite` or `core`) as well as the CLLI code for the site.
```
[amsterdam:vars]
type='lite'
clli='AMSTNL38'
```
Some hosts have host variables and may contain duplicate ip addresses (for example, see the `[amsterdam_tors]` group below, that duplicates and reorganizes the ip addresses for the hp irfs in Amsterdam). 
```
[amsterdam_hp]
10.84.127.33
10.84.127.51

[amsterdam_tors]
10.84.127.33  edge=10.84.127.51

[amsterdam_bs]
10.84.127.35
10.84.127.36
10.84.127.34
```
Every site has an `_idrac` group that contains the IPMI subnets for the site.
```
[amsterdam_idrac]
10.75.255.192/26
```

### Modules
The `library` folder contains the custom python modules used in the collection playbooks. These modules are the meat of the inventory collector process: they enable communication with the devices that ansible cannot communicate with out of the box and perform the logic that transforms the plain text output from the devices into machine-readable json output.

### Roles
The `roles` folder contains all the roles used in the inventory collection playbook. In general, each role connects to a type of device, runs commands, retrieves the result of those commands, formats the plain text into json, and sends a POST request with that json data to the formatter layer (inventory_store).

For a given role Ansible will set up variables to be used from the role's `vars` directory. It will then execute tasks listed in `main.yml` within the role's `tasks` directory.

Each role contains its own README.md to explain how it functions.

## Running Collectors
Collectors can be run from the command line manually or via Docker. To run via Docker, the image must first be set up. For first time setup, run `docker-compose up --build`. To update the image, run `docker-compose down && docker-compose up --build`.

When the image is built, run `docker-compose run ansible make inventory SITE=$site` where `$site` is the name of the site group in the inventory.ini file (ex: saopaulo). This will run the full site collector on whichever site you named. Replace `make inventory SITE=$site` with any of the make commands in the Makefile to run a single role.

### Make commands
The Makefile contains shortcuts for running full collection or individual roles. To run all collectors:
```
make inventory SITE=<site_name>
```
This is a shortcut that runs the following Ansible command which you can see in the Makefile:
```
ansible-playbook inventory.yml -i inventory.ini --extra-vars "site=$(SITE)"
```
To run individual collectors:
```
make inventory_<type> SITE=<site_name>
```
