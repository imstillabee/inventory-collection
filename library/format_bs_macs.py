#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to format output recieved from module into one object
"""

import json
import re
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    results=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

results = json.loads(MODULE.params['results'])

def format(stuff):
    output = []
    for result in stuff:
        if 'dpid' in result: 
            mac = result['dpid'][5:]
            mac = mac.replace(':', '')
            mac = '{0}-{1}-{2}'.format(mac[:4], mac[4:8], mac[8:])
            result['macAddress'] = mac
            output.append(result)
    return output

def main():
    formatted_macs = format(results)
    MODULE.exit_json(changed=False, meta=formatted_macs, log=formatted_macs)

if __name__ == '__main__':
    main()
