#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to put all ips retrieved from inventory formatter into a list
"""

import json
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    site_data=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

site_data = json.loads(MODULE.params['site_data'])


def create_list(data):
    ip_list = []
    for site in data:
        for region in site['regions']:
            for rack in region['racks']:
                for device in rack['devices']:
                    try:
                        new_host = dict(ip=device['ipAddress'])
                        if 'role' in device:
                            if device['role'] in [
                                    'Site', 'Edge', 'Agg', 'TOR']:
                                continue
                            new_host['group'] = device['role']
                        else:
                            continue
                        ip_list.append(new_host)
                    except KeyError:
                        continue
    return ip_list


def main():
    try:
        output = create_list(site_data)
    except Exception as e:
        MODULE.fail_json(msg='ERROR: {}'.format(e))
    MODULE.exit_json(changed=False, meta=output)


if __name__ == '__main__':
    main()
