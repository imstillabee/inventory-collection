#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to format output recieved from module into one object
"""

import json
import re
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    stdout=dict(required=True),
    site=dict(required=True),
    inventory=dict(required=True),
    ip=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

stdout = MODULE.params['stdout']
inventory = MODULE.params['inventory']
site = MODULE.params['site']
ip = MODULE.params['ip']

def format(results, inv):
    res = json.loads(results)
    output = dict()
    output['ipAddress'] = ip
    output['site'] = site
    output['collector'] = 'cisco'
    output['inventoryInfo'] = dict()
    for items in res:
        for aline in re.split("[\n]+", items):
            if re.match("^version ", aline) is not None:
                output['inventoryInfo']['softwareVersion'] = re.sub("^version\s", '', aline)
            if re.match("^hostname ", aline) is not None:
                output['inventoryInfo']['hostname'] = re.sub("^hostname\s", '', aline)
            if re.match("^\tChassis MAC Address", aline) is not None:
                output['macAddress'] = re.sub(".*Chassis MAC Address.*:\s+", '', aline).replace('.', '-')
            if re.match("Chassis Serial Number", aline) is not None:
                output['inventoryInfo']['serialNumber'] = re.sub(".*Chassis Serial Number.*:\s+", '', aline)
    for item in inv.split('\n\n'):
        device = dict()
        dev_type = None
        for attr in item.replace('\n', ', ').split(', '):
            if re.search("Power Supply", attr) is not None:
                dev_type = 'powerSupply'
                device['type'] = attr[attr.find(':')+1:].replace('"', '').strip()
            if re.search("Wan Interface", attr) is not None:
                dev_type = 'Nic'
                slotNum = re.search(r"Slot \d SubSlot \d", attr)
                if slotNum:
                    device['slotNumber'] = slotNum.group(0)
                modelNo = re.search(r"\((?P<model>.*)\)", attr)
                if modelNo:
                    device['modelNumber'] = modelNo.group('model')
            if re.match('PID', attr) is not None:
                device['partNumber'] = attr[attr.find(':')+1:].replace('"', '').strip()
            if re.match('VID', attr) is not None:
                device['versionNumber'] = attr[attr.find(':')+1:].replace('"', '').strip()
            if re.match('SN', attr) is not None:
                device['serialNumber'] = attr[attr.find(':')+1:].replace('"', '').strip()
        if dev_type is not None and dev_type not in output['inventoryInfo']:
            output['inventoryInfo'][dev_type] = []
        if len(device.keys()) > 0 and dev_type is not None:
            output['inventoryInfo'][dev_type].append(device)
    return output

def main():
    formatted = format(stdout, inventory)
    MODULE.exit_json(changed=False, meta=formatted)

if __name__ == '__main__':
    main()
