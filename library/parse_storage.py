#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to format output recieved from storage array module into one object
"""

import json
import re
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    results=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

results = json.loads(MODULE.params['results'])

HEADER = re.compile('\n(?P<header>[A-Z ]+)------------------------------')
SUBHEADER = re.compile('\n *(?P<subheader>[A-Z][A-Z ]{4,}) +\n')
CONTROLLER = re.compile(
    '(?P<controller>RAID Controller Module in Enclosure \d, Slot \d)')
SKIP = re.compile('Associated Virtual Disks|cephosd-\d\d_[A-Z]{3}\d\d,')
LIST_HEADS = re.compile(
    '(?P<head>Ethernet port|Physical Disk interface|Host interface):')
DISK_INFO = re.compile(
    'Physical Disk at Enclosure (?P<enclosure>\d+), ' +
    'Drawer (?P<drawer>\d+), Slot (?P<slot>\d+)[\v\s\w\W]*' +
    'Associated disk group:\s+(?P<disk_group>[\w\-_]+)[\v\s\w\W]*' +
    'Product ID:\s+(?P<model>\w+)[\v\s\w\W]*' +
    'firmware version:\s+(?P<firmware>\w+)[\v\s\w\W]*' +
    'Serial number:\s+(?P<serial>\w+)[\v\s\w\W]*' +
    'Manufacturer:\s+(?P<make>\w+)[\v\s\w\W]*' +
    'Date of manufacture:\s+(?P<mfg_date>[\w, ]+)')


def to_key_value(line_arr):
    master_dict = {}
    kv_pairs = {}
    last_key = ''
    for line in line_arr:
        try:
            k, v = re.split(': +', line)
        except ValueError as err:
            if str(err) == 'too many values to unpack':
                try:
                    k1, v1, k2, v2 = re.split(': +', line)
                except ValueError:
                    continue
                else:
                    newdict = {}
                    newdict.update(
                        {re.sub(' ', '_', k1.strip()).lower(): v1.strip()})
                    newdict.update(
                        {re.sub(' ', '_', k2.strip()).lower(): v2.strip()})
                    kv_pairs.update(newdict)
            elif len(line.strip()) > 0 and not len(
                    re.split(' {4,}', line)) >= 4:
                if last_key != '':
                    master_dict[last_key].update(kv_pairs)
                    kv_pairs = {}
                temp = re.sub(' ', '_', line.strip()).lower()
                if re.search('_____', temp) is not None:
                    continue
                last_key = temp
                master_dict.update({last_key: {}})
        else:
            kv_pairs.update({re.sub(' ', '_', k.strip()).lower(): v.strip()})
    if len(master_dict.keys()) == 1:
        master_dict[last_key].update(kv_pairs)
        return master_dict
    if len(master_dict.keys()) > 1:
        for key in master_dict.keys():
            if len(master_dict[key].keys()) == 0:
                del master_dict[key]
    if len(master_dict.keys()) > 0:
        return master_dict
    else:
        return kv_pairs


def format(result):
    output = {}
    sections = re.split(HEADER, result)
    sections = [[re.split('\n', sect) for sect in re.split(
                SUBHEADER, section)]for section in sections]
    output.update(to_key_value(sections[0][0]))
    sections = sections[1:]
    root_key = re.sub(' ', '_', sections[0][0][0].strip()).lower()
    output.update({root_key: {}})
    parsed = {}
    last_key = ''
    for subsec in sections[1]:
        if len(subsec) == 1:
            last_key = re.sub(' ', '_', subsec[0].strip()).lower()
            parsed.update({last_key: {}})
        elif last_key != '':
            parsed[last_key].update(to_key_value(subsec))
    output[output.keys()[len(output.keys())-1]].update(parsed)
    root_key = re.sub(' ', '_', sections[16][0][0].strip()).lower()
    output.update({root_key: {}})
    parsed = {}
    last_key = ''
    next_array = []
    empty_count = 0
    for line in sections[17][0][:-4]:
        if 'Overall' in line:
            if last_key != '':
                if isinstance(parsed[last_key], list):
                    parsed[last_key].append(to_key_value(next_array))
                else:
                    parsed[last_key].update(to_key_value(next_array))
                next_array = []
            last_key = re.sub(' ', '_', line.strip()).lower()
            parsed.update({last_key: {}})
        elif 'Detected' in line:
            if last_key != '' and len(next_array) > 0:
                if isinstance(parsed[last_key], list):
                    parsed[last_key].append(to_key_value(next_array))
                else:
                    parsed[last_key].update(to_key_value(next_array))
                next_array = []
            last_key = re.split(' ', line.strip())[0].lower()
            parsed.update({last_key: []})
        elif line == '':
            empty_count += 1
            if empty_count >= 2 and len(next_array) > 0:
                if last_key != '':
                    if isinstance(parsed[last_key], list):
                        parsed[last_key].append(to_key_value(next_array))
                        next_array = []
                        empty_count = 0
                    else:
                        parsed[last_key].update(to_key_value(next_array))
                        next_array = []
                        empty_count = 0
        else:
            next_array.append(line)
    output[root_key].update(parsed)
    root_key = re.sub(' ', '_', sections[12][0][0].strip()).lower()
    output.update({root_key: {}})
    parsed = {}
    last_key = ''
    next_array = []
    list_head = ''
    current_controller = ''
    for line in sections[13][0]:
        skip = SKIP.search(line)
        match = CONTROLLER.search(line)
        list_item = LIST_HEADS.search(line)
        if skip is not None:
            continue
        if match is None and current_controller == '':
            continue
        elif match is not None and current_controller == '':
            current_controller = re.sub(
                ' ', '_', match.group('controller').strip()).lower()
            parsed.update({current_controller: {}})
            continue
        elif list_item is not None and list_head == '':
            if len(next_array) > 1:
                parsed[current_controller].update(to_key_value(next_array))
            next_array = []
            next_array.append(line)
            list_head = re.sub(
                ' ', '_', list_item.group('head').strip()).lower()
            parsed[current_controller].update({list_head: []})
            continue
        elif list_item is not None and list_head == re.sub(
                ' ', '_', list_item.group('head').strip()).lower():
            parsed[current_controller][list_head].append(
                to_key_value(next_array))
            next_array = []
            continue
        elif list_item is not None and list_head != re.sub(
                ' ', '_', list_item.group('head').strip()).lower():
            if len(next_array) > 0:
                parsed[current_controller][list_head].append(
                    to_key_value(next_array))
            next_array = []
            next_array.append(line)
            list_head = re.sub(
                ' ', '_', list_item.group('head').strip()).lower()
            parsed[current_controller].update({list_head: []})
        elif match is None and current_controller != '':
            next_array.append(line)
            continue
        elif match is not None and current_controller != '':
            if list_head != '':
                parsed[current_controller][list_head].append(
                    to_key_value(next_array))
                list_head = ''
            else:
                parsed.update({current_controller: to_key_value(next_array)})
            next_array = []
            current_controller = re.sub(
                ' ', '_', match.group('controller').strip()).lower()
            parsed.update({current_controller: {}})
    if len(next_array) > 0:
        parsed[current_controller][list_head].append(to_key_value(next_array))
    output[root_key].update(parsed)
    disk_section_idx = sections.index([['DRIVES']]) + 1
    start_idx = sections[disk_section_idx][0].index('   DETAILS') + 1
    end_idx = sections[disk_section_idx][0].index(
        'DRIVE CHANNELS----------------------------')
    disk_lines = sections[disk_section_idx][0][start_idx:end_idx]
    disk_idxs = [i for i, line in enumerate(disk_lines) if re.search(
        'Enclosure \d, Drawer \d', line)]
    disks = []
    for i, idx in enumerate(disk_idxs):
        try:
            disks.append('\n'.join(disk_lines[idx:disk_idxs[i+1]]))
        except IndexError:
            disks.append('\n'.join(disk_lines[idx:]))
    output['physical_disks'] = []
    for disk in disks:
        match = DISK_INFO.search(disk)
        if match:
            output['physical_disks'].append(match.groupdict())
    return (output)


def main():
    formatted = format(results)
    mac_adds = []
    for key, value in formatted['controllers'].items():
        for port in value['ethernet_port']:
            if port['link_status'] == 'Up':
                mac = port['mac_address'].replace(':', '')
                mac_adds.append(
                    '{0}-{1}-{2}'.format(mac[:4], mac[4:8], mac[8:]))
    MODULE.exit_json(
        changed=False, meta=formatted, macs=mac_adds)


if __name__ == '__main__':
    main()
