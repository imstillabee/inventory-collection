#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to format output recieved from module into one object
"""

import json
import re
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    results=dict(required=True, type='list'),
    stdout=dict(required=True)
)
MODULE = AnsibleModule(argument_spec=FIELDS)
results = MODULE.params['results']
stdout = MODULE.params['stdout']


def to_keyval(input):
    try:
        k, v = input.split(" = ")
    except ValueError:
        return
    else:
        return dict({k: v})


def to_obj(input):
    output = {}
    for i in input:
        if (i[:1] == "["):
            _, k = i[1:-1].split(": ")
            output.update({k: {}})
        else:
            try:
                output[output.keys()[0]].update(to_keyval(i))
            except TypeError:
                continue
    return output


def format_helper(input):
    output = []
    for i in input:
        temp = []
        if (i == '-------------------------------------------------------------------'):
            output.append(temp)
            continue
        elif (i == '') or (i[:1] == '/'):
            continue
        else:
            temp.append(i)
        if (output.__len__() == 0):
            output.append(temp)
        else:
            output[output.__len__()-1].append(i)
    return output


def unflatten(dictionary):
    resultDict = dict()
    for key, value in dictionary.iteritems():
        parts = key.split(".")
        d = resultDict
        for part in parts[:-1]:
            if part not in d:
                d[part] = dict()
            d = d[part]
        d[parts[-1]] = value
    return resultDict


def format(results):
    arr = format_helper(results[4:])
    output = {}
    [output.update(to_obj(i)) for i in arr]
    return unflatten(output)


def main():
    # if len(results) < 1:
    #     MODULE.fail_json(msg='results.stdout_lines was empty')
    formatted = format(results)
    try:
        mac = formatted['iDRAC']['Embedded']['1-1#IDRACinfo']['PermanentMACAddress'].replace(':', '')
        mac_address = '{0}-{1}-{2}'.format(mac[:4],mac[4:8],mac[8:])
    except KeyError:
        MODULE.fail_json(msg='KeyError: iDRAC', stdout=stdout)
    MODULE.exit_json(changed=False, meta={'formatted': formatted, 'mac_address': mac_address})


if __name__ == '__main__':
    main()
