#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This is an ansible module to handle connecting and executing a command
    on a DEVICE.
"""

import re
import json
from ansible.module_utils.basic import AnsibleModule

try:
    from netmiko import ConnectHandler
    HAS_NETMIKO = True
except ImportError:
    HAS_NETMIKO = False

FIELDS = dict(
    device_type=dict(required=True, type='str'),
    ssh_host=dict(required=True, type='str'),
    ssh_username=dict(required=True),
    ssh_password=dict(required=True),
    ssh_port=dict(default=22, type='int'),
    cmd=dict(required=True, type='list'),
    config_mode=dict(required=True, type='bool')
)
MODULE = AnsibleModule(argument_spec=FIELDS)
DEVICE = {
    'device_type': MODULE.params['device_type'],
    'ip': MODULE.params['ssh_host'],
    'port': MODULE.params['ssh_port'],
    'username': MODULE.params['ssh_username'],
    'password': MODULE.params['ssh_password'],
    'timeout': 40
}

def run_command(data):
    """ -*- connect to HP -*- """
    device = ConnectHandler(**DEVICE)

    if (MODULE.params['config_mode'] == True):
        device.config_mode()
        result = [device.send_command_expect(i, delay_factor=2) for i in data['cmd']]
        device.exit_config_mode()
    else:
        result = [device.send_command_expect(i, delay_factor=2) for i in data['cmd']]

    return result


def main():
    """ -*- handle connection and response  -*- """

    if not HAS_NETMIKO:
        MODULE.fail_json(msg='The netmiko python MODULE is required')

    cmd = MODULE.params['cmd']

    if cmd and not isinstance(cmd, list):
        MODULE.fail_json(msg='Argument command must be a list.')

    try:
        if cmd:
            result = run_command(MODULE.params)
        else:
            pass
    except Exception as err:
        MODULE.fail_json(msg='ERROR: {}'.format(err))

    MODULE.exit_json(changed=False, meta=result)

if __name__ == '__main__':
    main()
