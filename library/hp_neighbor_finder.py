#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to parse output recieved from tor into list of mac addresses for
one cabinet
"""

import re
import json
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    data=dict(required=True),
    parents=dict(required=True, type='list')
)

MODULE = AnsibleModule(argument_spec=FIELDS)

data = json.loads(MODULE.params['data'])
parents = MODULE.params['parents']

IP = re.compile('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')


def check(sysname, parents, check_name, check_ip):
    if '5900' not in check_name and '5930' not in check_name:
        return False
    elif check_name == sysname:
        return False
    elif check_name in parents:
        return False
    elif not IP.match(check_ip):
        return False
    else:
        return True


def find(info, parents):
    sysname = info['lldp']['local']['global']['sys_name']
    neighbors = info['lldp']['neighbors']
    output = [dict(
        ip=neighbor['mgmt_addr'],
        name=neighbor['sysname']) for neighbor in neighbors if check(
            sysname, parents, neighbor['sysname'], neighbor['mgmt_addr'])]
    return output


def main():
    try:
        neighbors = find(data, parents)
    except Exception as e:
        MODULE.fail_json(msg='Error: {}'.format(e))
    MODULE.exit_json(changed=False, meta=neighbors)


if __name__ == '__main__':
    main()
