#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to parse output recieved from tor into list of mac addresses for
one cabinet
"""

import re
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    ip=dict(required=True),
    neighbors=dict(required=True),
    local=dict(required=True),
    manuinfo=dict(required=True),
    mac_adds=dict(required=True),
    arp=dict(required=True),
    interfaces=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

ip = MODULE.params['ip']
neighbors = MODULE.params['neighbors']
local = MODULE.params['local']
manuinfo = MODULE.params['manuinfo']
mac_adds = MODULE.params['mac_adds']
arp = MODULE.params['arp']
interfaces = MODULE.params['interfaces']

SYSTEM = re.compile('Global LLDP local-information:\n Chassis ID +: +(?P<mac_addr>[a-z0-9\-]+)\n System name +: +(?P<sys_name>[\w\-]+)(?:.*\n){5,16}.SoftwareRev\s+:\s+(?P<software>.+)\n.SerialNum\s+:\s+(?P<serial>[A-Z0-9]+)\n.Manufacturer name\s+:\s+(?P<manufacturer>\w+)\n.Model name\s+:\s+(?P<model>.+)')
LOCAL = re.compile('LLDP local-information of port \d+\[(?P<type>[a-zA-Z\-]+)(?P<interface>(?:\d/0/\d+)|\d+)(?:.*\n){1,4} Port description +: (?P<name>.*)(?:.*\n){1,4} Management address\s+:\s+(?P<addr>\d+\.\d+\.\d+\.\d+|.{4}\-.{4}\-.{4})')
NEIGHBOR = re.compile('LLDP neighbor-information of port \d+\[(?P<int_type>[a-zA-Z\-]+)(?P<interface>\d/0/\d+)(?:.*\n){,8}.Chassis ID\s+:\s+(?P<chassis_id>[a-z0-9\-]+)(?:.*\n){,2}.Port ID\s+:\s+(?P<port>[a-zA-z\d/\-]+)(?:.*\n){,6}.System name\s+:\s+(?P<sysname>.+)\n.System description\s+:\s+(?P<sys_description>.*)(?:(?:.*\n){,10}.Management address\s+:\s+(?P<mgmt_addr>[a-z0-9\.\-]+))?')
INTERFACE = re.compile('(?P<interface>Bridge-Aggregation\d+|[A-Za-z]+\d/0/\d+)\nCurrent state: (?P<state>[\w ]+)\n(?:.*\n){,3}Description: (?P<description>[\w_/\|\-]+)')
SLOT = re.compile(r'Slot (?P<slotnum>\d)')
COMPONENT = re.compile(r'(?P<comptype>Fan|Power) \d')


def parse_arp(text):
    lines = re.split('\n', text)
    lines = lines[1:]
    keys = re.split('  +', lines.pop(0))
    keys.append(keys[len(keys)-1].split()[1])
    keys[len(keys)-2] = keys[len(keys)-2].split()[0]
    arp_list = [dict(zip(keys, line.split())) for line in lines if len(line) > 0]
    return arp_list


def parse_mac_adds(macs):
    lines = macs.split('\n')
    keys = re.split('  +', lines.pop(0))
    keys[0] = keys[0].replace(' ', '_')
    keys[1] = keys[1].replace(' ', '_')
    mac_list = [dict(zip(keys, line.split())) for line in lines if (len(line) > 0)]
    return mac_list


def parse_manuinfo(text):
    if text == 'False':
        return text
    parsed = []
    lines = text.split('\n')
    cur_slot = dict()
    cur_dict = dict()
    cur_type = ''
    cur_arr = []
    for line in lines:
        print line
        if len(line) == 0:
            continue
        slot_match = SLOT.search(line)
        comp_match = COMPONENT.search(line)
        if slot_match is not None:
            if len(cur_dict.keys()) > 0:
                cur_slot[cur_type].append(cur_dict)
            if len(cur_slot.keys()) > 0:
                parsed.append(cur_slot)
            cur_slot = dict(slot=slot_match.group('slotnum'))
            cur_dict = {}
            cur_type = ''
            continue
        elif comp_match is not None:
            if len(cur_dict.keys()) > 0:
                cur_slot[cur_type].append(cur_dict)
            cur_type = comp_match.group('comptype')
            cur_dict = {}
            if cur_type not in cur_slot:
                cur_slot[cur_type] = []
        else:
            key_val = line.split(':')
            if cur_type == '':
                cur_slot[key_val[0].strip().replace(
                    ' ', '_').lower()] = key_val[1].strip()
            else:
                cur_dict[key_val[0].strip().replace(
                    ' ', '_').lower()] = key_val[1].strip()
    if len(cur_dict.keys()) > 0:
        cur_slot[cur_type].append(cur_dict)
    if len(cur_slot.keys()) > 0:
        parsed.append(cur_slot)
    return parsed


def parse_interfaces(text):
    matches = re.finditer(INTERFACE, text)
    hp_interfaces = [match.groupdict() for match in matches]
    return hp_interfaces


def parse_neighbors(data):
    matches = re.finditer(NEIGHBOR, data)
    neighbors = [match.groupdict() for match in matches]
    return neighbors


def parse_local(text):
    output = dict()
    match = SYSTEM.search(text)
    if match:
        sys_lldp = match.groupdict()
        output['global'] = sys_lldp
    else:
        raise Exception('Unable to find system local lldp info')
    matches = re.finditer(LOCAL, text)
    output['local'] = [match.groupdict() for match in matches]
    return output


def parse(neighbors, local, manuinfo, mac_adds, arp, interfaces, ip_addr):
    output = dict(
        ip=ip_addr,
        lldp=dict(
            neighbors=parse_neighbors(neighbors),
            local=parse_local(local)),
        irf=parse_manuinfo(manuinfo),
        mac_adds=parse_mac_adds(mac_adds),
        arp=parse_arp(arp),
        interfaces=parse_interfaces(interfaces))
    return output


def main():
    try:
        formatted = parse(
            neighbors, local, manuinfo, mac_adds, arp, interfaces, ip)
    except Exception as e:
        MODULE.fail_json(msg='Error: {}'.format(e))
    MODULE.exit_json(changed=False, meta=formatted)


if __name__ == '__main__':
    main()
