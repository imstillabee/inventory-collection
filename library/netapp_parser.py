
"""
    Module to parse output recieved from netapp devices
"""

import paramiko
import time
import re
import json
from ansible.module_utils.basic import AnsibleModule
from itertools import chain

FIELDS = dict(
    ip_addr=dict(required=True, type='str'),
    ssh_user=dict(required=True, type='str'),
    ssh_pass=dict(required=True, type='str'),
)

MODULE = AnsibleModule(argument_spec=FIELDS)

ip_addr = MODULE.params['ip_addr']
ssh_user = MODULE.params['ssh_user']
ssh_pass = MODULE.params['ssh_pass']

lines = []

def remote_ssh():
    output = dict()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect( hostname = ip_addr , username = ssh_user, password = ssh_pass)
    channel = ssh.invoke_shell()
    channel.send('network port show -fields mac -port e0M\n')
    time.sleep(2)
    if channel.recv_ready():
        mac = format_mac((channel.recv(9999)))
    channel.send('system controller show -node * -fields serial-number, model, system-id\n')
    time.sleep(2)
    if channel.recv_ready():
        controller = format_controller((channel.recv(9999)))
    channel.send('cluster identity show\n')
    time.sleep(2)
    if channel.recv_ready():
        cluster = format_cluster((channel.recv(9999)))
    channel.send('system node run -node  *01 sysconfig  -A\n')
    time.sleep(10)
    if channel.recv_ready():
        components_n1 = format_sysconfig((channel.recv(9999)))
    channel.send('system node run -node  *02 sysconfig  -A\n')
    time.sleep(10)
    if channel.recv_ready():
        components_n2 = format_sysconfig((channel.recv(9999)))
    output['controller2'] = dict(mac=mac['node02'], **controller['node02'])
    output['controller1'] = dict(mac=mac['node01'], **controller['node01'])
    output['controller2'] = dict(components=components_n2, **output['controller2'])   
    output['controller1'] = dict(components=components_n1, **output['controller1'])
    output = dict(output, **cluster)

    return output
    
def format_cluster(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(2, len(lines[:5])):
        substring1 = lines[i].find(':')
        pos = substring1+len(':')
        key = lines[i][:substring1].strip()
        value = lines[i][pos:].strip()
        if len(value)>0:
            obj[key] = value
    return obj

def format_controller(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(3, len(lines[:5])):
        part = {}
        t = filter(None, lines[i].split())
        keys = filter(None, lines[1].split())
        for j in range( 0,len(t)): 
            key = keys[j]
            value = t[j]
            part[key] = value
        substring1 = lines[i].find(' ')
        pos = substring1 + len(' ')
        node = 'node' + t[0][-2:] 
        obj[node]= part
    return obj

def format_mac(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(3, len(lines[:5])):
        substring1 = lines[i].find('e0M')
        pos = substring1+len('e0M')
        key = lines[i][:substring1].replace(lines[i][:substring1], 'node'+lines[i][substring1-3:substring1]).strip()
        value = lines[i][pos:].strip()
        value = '{0}-{1}-{2}'.format(value.replace(':', '')[:4], value.replace(':', '')[4:8], value.replace(':', '')[8:])
        if len(value)>0:
            obj[key] = value
    return obj

def format_disk(a, b, lines):
    obj={}
    slot={}
    components =[]
    for i in range(a, len(lines[:b+1])): 
        stri = lines[i].replace('    ', '            ')
        value = stri[-20:].strip()
        components.append(value)
        if len(value)>0:
            slot['serialnum'] = components
            obj['disk'] = slot  
    return obj

def format_sysconfig(string):
    pos=string.find('\r\n1 entry was acted on.')
    lines = string[pos:].split('\r\n\t')
    lines = string[pos:].split('\r\n')  
    obj = dict(chain(parse_interface(7, 7, lines).items(), parse_interface(4, 4, lines).items(), parse_interface(11, 14, lines).items(), parse_interface(14, 22, lines).items(),parse_interface(23, 27, lines).items(), parse_interface(28, 32, lines).items(), parse_interface(33, 38, lines).items(), parse_interface(39, 43, lines).items(), parse_interface(46, 49, lines).items(),parse_interface(50, 50, lines).items(), format_disk(55, 78, lines).items()))
    
    return obj

def parse_interface(a, b, lines):
    obj={}
    slot={}
    components = []
    slot0 = ''
    for i in range(a, len(lines[:b+1])):   
        if i == a and lines[i].find('slot 0:') != -1:
            slot0 = lines[i].replace('slot 0:','').strip()
        substring1 = lines[i].find(':')
        pos = substring1+len(':')
        key = lines[i][:substring1].strip().replace('slot 0','').replace('           ',' ')
        value = lines[i][pos:].strip().replace('           ',' ')
        if key.find('.') != -1:
            substring1 = key.rfind(' ')
            pos2 = substring1-len('.')
            value = key[substring1:].strip()
            key = key[:substring1]
            obj[key] = value
        if i == 39:
            key = slot0[:18].replace(':','').strip()
            obj[key] = slot0[19:].strip()
            continue
        if len(key)>0 and len(value)>0 and i <41:
            if  slot0 == '' :
                slot0 = key
            slot[key] = value
            obj[slot0] = slot     
        elif len(key) == 0 and i>39:
            if i == 50:
                slot['info'] = lines[51].replace('\t',' ').strip()
                key = lines[i].replace('slot 0:',' ').strip()
                obj[key] = {'info': lines[51].replace('\t',' ').strip()}
            else:
                value = lines[i][-3:].strip()
                key = lines[i][:-3].replace('slot 0:','').strip()
                components.append(value)
                obj[key] = components
    return obj

def main():
    try:
        result = remote_ssh()
        formatted = format(result)
    except Exception as err:
        MODULE.fail_json(meta=[result], msg='ERROR: {}'.format(err), log=lines, check=str(len(lines)))
    MODULE.exit_json(changed=False, meta=[result], log=lines, check=str(len(lines)))

if __name__ == '__main__':
    main()
