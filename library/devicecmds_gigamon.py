"""
    Module to parse output recieved from gigamon devices
"""

import paramiko
import time
import re
import json
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    ip_addr=dict(required=True, type='str'),
    ssh_user=dict(required=True, type='str'),
    ssh_pass=dict(required=True, type='str'),
)

MODULE = AnsibleModule(argument_spec=FIELDS)

ip_addr = MODULE.params['ip_addr']
ssh_user = MODULE.params['ssh_user']
ssh_pass = MODULE.params['ssh_pass']

def remote_ssh():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect( hostname = ip_addr, username = ssh_user, password = ssh_pass)
    channel = ssh.invoke_shell()
    time.sleep(5)
    channel.send('enable\n')
    channel.send('show version\n')
    channel.send(':q\n')
    time.sleep(5)
    if channel.recv_ready():
        version = format_version((channel.recv(9999)))  
    channel.send('show chassis\n')
    channel.send(':q\n')
    time.sleep(5)
    if channel.recv_ready():
        chassis = format_chassis((channel.recv(9999)))  
    channel.send('show card\n')
    time.sleep(5)
    if channel.recv_ready():
        card = format_card((channel.recv(9999)))
    channel.send('show interfaces eth0\n')
    time.sleep(10)
    if channel.recv_ready():
        mac = format_mac((channel.recv(9999)))  
    output = [dict(mac, **version), chassis, card]
    return output

def format_card(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(5, len(lines)):
        part = {}
        t = filter(None, lines[i].split())
        keys = filter(None, lines[3].split('  '))
        for j in range( 0,len(t)): 
            key = keys[j]
            value = t[j]
            part[key] = value
        obj[t[3]]= part
    return obj

def format_mac(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(8, len(lines)):
        key = lines[i][3:22].lower().replace(' ', '-').replace(':','').strip('-')
        value = lines[i][23:]
        if len(value)>0:
            obj[key] = value
    return obj

def format_version(string):
    obj = {}
    lines = string.split('\r\r\n')
    lines = string.split('\r\n')
    lines.pop()
    for i in range(2, len(lines)):
        key = lines[i][0:18].lower().replace(' ', '-').replace(':','').strip('-')
        value = lines[i][19:]
        if len(value)>0:
            obj[key] = value
    return obj

def format_chassis(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(4, len(lines[:15])):
        key = lines[i][:20].lower().replace(' ', '-').replace(':','').strip('-')
        value = lines[i][22:]
        if len(value)>0:
            obj[key] = value
    return obj

def main():
    result = {} 
    try:
        output = remote_ssh()
        spec_output=output[0]
        card_output = output[2]
        mac = spec_output['hw-address']
        mac='{0}-{1}-{2}'.format(mac.replace(':', '')[:4], mac.replace(':', '')[4:8], mac.replace(':', '')[8:])
        result = {
            'Chassis':output[1],
            'mac_address': mac.lower(),
            'Speed': spec_output['speed'],
            'System memory': spec_output['system-memory'],
            'Version-Summary': spec_output['version-summary'],
            'Product model': spec_output['product-model'],
            'Product hw': spec_output['product-hw'],
            'Card': output[2]
        }     
    except Exception as err:
        MODULE.fail_json(msg='ERROR: {}'.format(err), log=result, check=result)
    MODULE.exit_json(changed=False, meta=[result] )

if __name__ == '__main__':
    main()
