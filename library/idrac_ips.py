#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to put all ips retrieved from inventory formatter into a list
"""

import json
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    site_data=dict(required=True),
    ip_ranges=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

site_data = json.loads(MODULE.params['site_data'])
ip_ranges = json.loads(MODULE.params['ip_ranges'])

octet_list = ['.'.join(ip.split('.')[:2]) for ip in ip_ranges]
octets = set(octet_list)


def create_list(data):
    ip_list = []
    for region in data['regions']:
        for rack in region['racks']:
            for device in rack['devices']:
                try:
                    if 'role' in device:
                        continue
                    ip = device['ipAddress']
                    if '.'.join(ip.split('.')[:2]) in octets:
                        ip_list.append(dict(
                            ip=device['ipAddress'],
                            security=False))
                    elif rack['name'] == 'security' or rack['name'] == 'utility':
                        ip_list.append(dict(
                            ip=device['ipAddress'],
                            security=True))
                except KeyError:
                    continue
    return ip_list


def main():
    output = create_list(site_data[0])
    MODULE.exit_json(changed=False, meta=output)


if __name__ == '__main__':
    main()
