#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module to format unicode string to json for posting to formatter api
"""

import re
from ast import literal_eval
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    data=dict(required=True),
    site=dict(required=True),
    clli=dict(required=True)
)

MODULE = AnsibleModule(argument_spec=FIELDS)

data = literal_eval(re.sub('Undefined', 'None', MODULE.params['data']))
site = MODULE.params['site']
clli = MODULE.params['clli']


def to_json(info, sitename, code):
    hp_list = [item for item in data if item]
    output = dict(
        site=sitename,
        clli=code,
        discovery=hp_list)
    return output


def main():
    try:
        formatted = to_json(data, site, clli)
    except Exception as e:
        MODULE.fail_json(msg='Error: {}'.format(e))
    MODULE.exit_json(changed=False, meta=formatted)


if __name__ == '__main__':
    main()
