import paramiko
import time
from ansible.module_utils.basic import AnsibleModule

FIELDS = dict(
    ssh_host=dict(required=True, type='str'),
    ssh_user=dict(required=True, type='str'),
    ssh_pass=dict(required=True, type='str'),
)

MODULE = AnsibleModule(argument_spec=FIELDS)

ssh_host = MODULE.params['ssh_host']
ssh_user = MODULE.params['ssh_user']
ssh_pass = MODULE.params['ssh_pass']


def remote_ssh():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=ssh_host, username=ssh_user, password=ssh_pass)
    channel = ssh.invoke_shell()
    time.sleep(10)
    if channel.recv_ready():
        hostname = format_hostname(channel.recv(9999))
    channel.send('show environment\n')
    time.sleep(10)
    if channel.recv_ready():
        environment = format_environment((channel.recv(9999)))
    channel.send('show version\n')
    time.sleep(2)
    if channel.recv_ready():
        version = format_version((channel.recv(9999)))
    bcf = version['version']
    channel.send('enable\n')
    time.sleep(2)
    channel.send('debug bash\n')
    time.sleep(2)
    if bcf[:1] == '4':
        channel.send('ifconfig | grep bond0\n')
    elif bcf[:1] == '3':
        channel.send('ifconfig | grep eth0\n')
    time.sleep(2)
    if channel.recv_ready():
        mac_address = format_mac((channel.recv(9999)))
    channel.send('exit\n')
    time.sleep(2)
    return dict(
        hostname.items() + environment.items() +
        version.items() + mac_address.items())


def format_hostname(string):
    obj = {}
    lines = string.split('\r\n')
    obj['hostname'] = lines[len(lines)-1].replace('>', '').replace(' ', '')
    return obj


def format_mac(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    raw_mac = lines[len(lines) - 1].strip(' ')[-17:]
    mac = raw_mac.replace(':', '')
    obj['macAddress'] = '{0}-{1}-{2}'.format(mac[:4], mac[4:8], mac[8:12])
    return obj


def format_environment(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(8, len(lines)):
        key = lines[i][3:22].lower().replace(' ', '-').strip('-')
        value = lines[i][23:]
        obj[key] = value
    return obj


def format_version(string):
    obj = {}
    lines = string.split('\r\n')
    lines.pop()
    for i in range(2, len(lines)):
        key = lines[i][0:18].lower().replace(' ', '-').strip('-')
        value = lines[i][20:]
        obj[key] = value
    return obj


def main():
    output = {}
    try:
        result = remote_ssh()
        output['macAddress'] = result['macAddress']
        output['inventoryInfo'] = {
            'hostname': result['hostname'],
            'service-tag': result['serial-number'],
            'platform': result['platform'],
            'version': result['version'],
            'release': result['release-string'],
            'build-date': result['build-date'],
        }

    except Exception as err:
        MODULE.fail_json(msg='ERROR: {}'.format(err), log=output, check=output)

    MODULE.exit_json(changed=False, meta=output)


if __name__ == '__main__':
    main()
