#!/bin/bash

# Setup Path
PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin
export PATH

# Set Variables
site=$1
date=$(date +%Y-%m-%d)

# Run Collector
cd /home/ubuntu/inventory-automation
docker-compose run --name discovery_$site ansible make discovery SITE=$site 2>&1 > /home/ubuntu/logs/${site}-${date}-discovery.log
wait
docker-compose run --name inventory_$site ansible make inventory SITE=$site 2>&1 > /home/ubuntu/logs/${site}-${date}-inventory.log
wait
docker-compose run --name idrac_$site ansible make inventory_idrac SITE=$site 2>&1 > /home/ubuntu/logs/${site}-${date}-idrac.log
wait
docker-compose run --name push_$site ansible make site_push SITE=$site 2>&1 > /home/ubuntu/logs/${site}-${date}-push.log
