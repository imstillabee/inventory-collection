#!/bin/bash

# Setup Path
PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin
export PATH

# stop containers
docker stop $(docker ps -a -q)

# remove stopped containers
docker rm $(docker ps -a -q)

# rebuild docker image
cd /home/ubuntu/inventory-automation/
docker-compose up --build
